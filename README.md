Webpack boilerplate with PostCSS, SASS, Handlebars and Babel. Used to create simple single page sites, holding pages or as a starting point for more complex apps.

Based on https://github.com/taniarascia/webpack-boilerplate

## Build
    npm run build

## Server
    npm start