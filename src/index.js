// Test import of a JavaScript module
import { example } from '@/js/example'

// Test import of an asset
import staticImg from '@/static/images/example.png'

// Test import of styles
import '@/styles/index.sass'

// Appending to the DOM
const heading = document.createElement('p')
heading.textContent = example()

// Test a background image url in CSS
const imageBackground = document.createElement('div')
imageBackground.classList.add('image')

// Test a static folder asset
const imageStatic = document.createElement('img')
imageStatic.src = staticImg

// Test a public folder asset
const imagePublic = document.createElement('img')
imagePublic.src = '/assets/example.png'

const app = document.querySelector('#root')
app.append(heading, imageBackground, imageStatic, imagePublic)
